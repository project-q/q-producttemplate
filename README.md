# Product Template Service

Product Template-Service stellt Produkt-Vorlagen/Daten zur Verfügung, damit der Erzeuger die Produkte nicht immer von neuem Einstellen muss.

![Input section](/documentation/img/backend-stack.png "Backend Stack")

![Input section](/documentation/img/product-template-service-step3.png "Architecture")

## Usage

### Backend

Mit der Verwendung von Nodemon:
```bash
npm start
```

Um die Daten zur Verfügung zu stellen muss der Server gestartet werden und lokal aufgerufen werden (gilt auch wenn Daten über Frontend abgerufen werden sollen)

Backend / die Daten:
http://localhost:3001/graphql

![Input section](/documentation/img/graphql-server.png "graphql server")

![Input section](/documentation/img/mongodb.png "mongodb")


### Frontend

Das Frontend kann über das Repository Q-Frontend (Branch: q-product-template) abgerufen werden:
https://gitlab.com/project-q/q-frontend/tree/q-product-template


Frontend / über Q-Frontend:
http://localhost:3000/dashboard/add-product
```bash
npm start
```
Code-Bereich im Frontend zu den Daten: Q-Frontend/src/dashboard/components

Im Frontend hat der Erzeuger jetzt die Entschiedung zwischen Basis Templates oder seinen eigenen individuellen Templates zu wählen. Somit kann der Erzeuger Produkte die geupdatet werden müssen und/oder noch einmal in den Shop rein gestellt werden müssen über "meine Templates" schneller aufrufen. Will der Erzeuger ein ganz neues Produkt einstellen, so kann er sich aus den Basis Templates bedienen.

---

![Input section](/documentation/img/product-template-1.png "Add Product")

---

![Input section](/documentation/img/product-template-2.png "Alle Produkt Templates")

---

![Input section](/documentation/img/product-template-3.png "Eigene Produkt Templates")

---
