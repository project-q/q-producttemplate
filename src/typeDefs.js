import { gql } from 'apollo-server';

export const typeDefs = gql`
  type Query{
    """ Gets all basis templates. """
    getAllBasisTemplates: [BasisTemplate!]!

    """ Gets basis template for a given ID. """
    getBasisTemplateById(_id: ID!): BasisTemplate!
    
    """ Get specific template for a given ID. """
    getSpecificTemplateById(_id: ID!): SpecificTemplate
    
    """ Gets templates for a given producer. """
    getTemplatesByProducer(producerId: String!): [SpecificTemplate!]!

    """ Gets basis templates for a given title. """
    getBasisTemplatesByTitle(title: String!): [BasisTemplate!]

    """ Gets specific templates for a given title and producerId. """
    getSpecificTemplatesByTitle(title: String!, producerId: ID!): [SpecificTemplate!]
  }

  type Mutation{
    """ Creates a basis template for a valid role. """
    createBasisTemplate(input: CreateTemplateInput): BasisTemplate
    
    """ Deletes a basis template, if producer is owner of the basis template. """
    deleteBasisTemplate(_id: ID!): BasisTemplate
    
    """ Updates a basis template, if producer is owner of the basis template. """
    updateBasisTemplate(_id: ID!, input: UpdateTemplateInput): BasisTemplate
    
    """ Creates a specific template for a valid role. """
    createSpecificTemplate(input: CreateSpecificTemplateInput): SpecificTemplate
    
    """ Deletes a specific template, if producer is owner of the specific template. """
    deleteSpecificTemplate(_id: ID!, producerId: String!): SpecificTemplate
    
    """ Updates a specific template, if producer is owner of the specific template. """
    updateSpecificTemplate(_id: ID!, input: UpdateSpecificTemplateInput): SpecificTemplate

    """ Deletes all templates from a given producer. """
    deleteSpecificTemplates(producerId: ID!): [SpecificTemplate]
  }

  type BasisTemplate @key(fields: "_id"){
    _id: ID!
    title: String!
    description: String!
    longDescription: String
    img: String!
    images: [String]
    offerTypes: [TemplateOfferType!]
    category: Category
    allergen: [Allergen]
    additive: [Additive]
    foodSafety: [FoodSafety]
    color: String
    packaging: Packaging
    condition: Condition
  }

  type SpecificTemplate @key(fields: "_id"){
    _id: ID!
    producerId: String!
    title: String!
    description: String!
    longDescription: String
    img: String!
    images: [String]
    offerTypes: [TemplateOfferType!]
    category: Category
    allergen: [Allergen]
    additive: [Additive]
    foodSafety: [FoodSafety]
    color: String
    packaging: Packaging
    condition: Condition
  }

  type TemplateOfferType {
    price: Float
    totalAmount: Float
    packagingAmount: Float
    templateUnit: TemplateUnit
    tax: Float
    discount: Float
  }

  input CreateTemplateInput{
    title: String!
    description: String!
    longDescription: String
    img: String!
    images: [String]
    offerTypes: [TemplateOfferTypeInput]
    category: Category
    allergen: [Allergen!]
    additive: [Additive!]
    foodSafety: [FoodSafety!]
    color: String
    packaging: Packaging
    condition: Condition
  }

  input CreateSpecificTemplateInput{
    producerId: ID!
    title: String!
    description: String!
    longDescription: String
    img: String!
    images: [String]
    offerTypes: [TemplateOfferTypeInput]
    category: Category
    allergen: [Allergen!]
    additive: [Additive!]
    foodSafety: [FoodSafety!]
    color: String
    packaging: Packaging
    condition: Condition
  }

  input TemplateOfferTypeInput{
    price: Float
    totalAmount: Float
    packagingAmount: Float
    tempalteUnit: TemplateUnit
    tax: Float
    discount: Float
  }

  input UpdateTemplateInput{
    title: String
    description: String
    longDescription: String
    img: String
    images: [String]
    offerTypes: [TemplateOfferTypeInput]
    category: Category
    allergen: [Allergen]
    additive: [Additive]
    foodSafety: [FoodSafety]
    color: String
    packaging: Packaging
    condition: Condition
  }

  input UpdateSpecificTemplateInput{
    producerId: ID!
    title: String
    description: String
    longDescription: String
    img: String
    images: [String]
    offerTypes: [TemplateOfferTypeInput]
    category: Category
    allergen: [Allergen]
    additive: [Additive]
    foodSafety: [FoodSafety]
    color: String
    packaging: Packaging
    condition: Condition
  }

  enum TemplateUnit{
    QUANTITY
    LITER
    KILOGRAM
    GRAM
  }

  enum Category{
    CEREAL
    CEREAL_PRODUCTS
    POTATO
    POTATO_PRODUCTS
    FRUIT
    FRUIT_PRODUCTS
    DRIED_FRUIT
    VEGETABLES
    PULSES
    NUTS
    SEEDS
    MEAT
    MEAT_PRODUCTS
    FISH
    FISH_PRODUCTS
    MILK
    DAIRY_PRODUCTS
    EGG
    EGG_DISHES
    OILS
    FATS
    CONFECTIONERY
    SUGAR
    PRESERVES
    READY_MEALS
    SAUCES
    SEASONINGS
    SPICES
    HERBS
    NON_ALCOHOLIC
    ALCOHOLIC
  }

  enum Allergen {
    GLUTEN
    CRUSTACEAN
    EGG
    FISH
    PEANUTS
    SOYBEAN
    MILK
    LACTOSE
    NUT
    CELERY
    MUSTARD
    SESAME_SEEDS
    SULFUR_DIOXIDE
    SULFITE
    LUPIN
  }

  enum Additive {
    DYE
    PRESERVATIVE
    ANTIOXIDANT
    FLAVOUR_ENHANCER
    SULFURED
    BLACKENED
    WAXED
    PHOSPHATE
    SWEETENER
    PHENYLALANINE
  }

  enum FoodSafety {
    ALCOHOL
    GELATIN
    PORK
    MEATLESS
  }

  enum Packaging {
    PLASTIC
    PAPER
  }

  enum Condition {
    MOIST
    FROZEN
    COOLED
    DRIED
    RTP
    RIPE
    RAW
    FERMENTED
  }
`;
