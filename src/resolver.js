// import { Template } from './models/template';
const db = require('./utils/db')
import { ObjectId } from 'mongodb'
const shortid = require('shortid')

export const resolvers = {
  Query: {
    getAllBasisTemplates: (_parent, _args, _context, _info) => db.collection('product-templates').find({}).toArray(),
    getBasisTemplateById: (parent, args, _context, _info) => {
      const basisTemplateId = args._id
      return db.collection('product-templates').findOne({ _id: basisTemplateId })
    },
    getSpecificTemplateById: (parent, args, _context, _info) => {
      const specificTemplateId = args._id
      return db.collection('specific-product-templates').findOne({ _id: specificTemplateId })
    },
    getTemplatesByProducer: (_parent, args, _context, _info) => {
      const { producerId } = args
      return db.collection('specific-product-templates').find({ producerId: producerId }).toArray()
    },
    getBasisTemplatesByTitle: (_parent, args, _context, _info) => {
      const { title } = args
      let translatedTitle = translateTitle(title)
      if (!translatedTitle) return
      return db.collection('product-templates').find({ title: { $regex: `.*${translatedTitle}.*`, $options: 'i' } }).toArray()
    },
    getSpecificTemplatesByTitle: (_parent, args, _context, _info) => {
      const { title, producerId } = args
      let translatedTitle = translateTitle(title)
      if (!translatedTitle) return
      return db.collection('specific-product-templates').find({ title: { $regex: `.*${translatedTitle}.*`, $options: 'i' }, producerId }).toArray()
    }
  },
  Mutation: {
    createSpecificTemplate: async (_parent, args, context, _info) => {
      const { input } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == input.producerId || user.accountType == 'ADMIN') {
        input._id = shortid.generate()
        await db.collection('specific-product-templates').insertOne(input)
        return input
      } else {
        return null
      }
    },
    updateSpecificTemplate: async (_parent, args, context, _info) => {
      const { _id, input } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == input.producerId || user.accountType == 'ADMIN') {
        await db.collection('specific-product-templates').findOneAndUpdate({ _id: _id }, { $set: input })
        return db.collection('specific-product-templates').findOne({ _id: _id })
      } else {
        return null
      }
    },
    deleteSpecificTemplate: async (_parent, args, context, _info) => {
      const { _id, producerId } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == producerId || user.accountType == 'ADMIN') {
        await db.collection('specific-product-templates').deleteOne({ _id: _id })
        return null
      } else {
        return null
      }
    },
    deleteSpecificTemplates: async (_parent, args, context, _info) => {
      const { producerId } = args
      await db.collection('specific-product-templates').deleteMany({ producerId })
    },
    createBasisTemplate: async (_parent, args, context, _info) => {
      const { input } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.accountType == 'ADMIN') {
        input._id = shortid.generate()
        await db.collection('product-templates').insertOne(input)
        return input
      } else {
        return null
      }
    },
    updateBasisTemplate: async (_parent, args, context, _info) => {
      const { _id, input } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.accountType == 'ADMIN') {
        await db.collection('product-templates').findOneAndUpdate({ _id: _id }, { $set: input })
        return db.collection('product-templates').findOne({ _id: _id })
      } else {
        return null
      }
    },
    deleteBasisTemplate: async (_parent, args, context, _info) => {
      const { _id } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.accountType == 'ADMIN') {
        await db.collection('product-templates').deleteOne({ _id: _id })
        return null
      } else {
        return null
      }
    }
  }
}

// TODO add REAL translation
function translateTitle(englishTitle) {
  let mappedProducts = {
    "banana": "Banane",
    "strawberry": "Erdbeer",
    "broccoli": "Brokkoli",
    "custard apple": "Apfel",
  }
  return (mappedProducts[englishTitle]) ? mappedProducts[englishTitle] : null
}
