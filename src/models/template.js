import mongoose from 'mongoose';

var schema = new mongoose.Schema({
  producerID: {
    type: String
  },
  title: {
    type: String
  },
  description: {
    type: String
  },
  img: {
    type: String
  },
  category: {
    type: String
  },
  images: [{
    type: String
  }],
  allergen: [{
    type: String
  }],
  additive: [{
    type: String
  }],
  foodSafety: [{
    type: String
  }],
  packaging: {
    type: String
  },
  condition: {
    type: String
  }
})

export const Template = mongoose.model('Template', schema)
