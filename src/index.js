import express from 'express';
import mongoose from 'mongoose';
import { ApolloServer, gql , PubSub } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import { resolvers } from './resolver';
import { typeDefs } from './typeDefs';
const db = require('./utils/db');
import config from 'config';

const port = process.env.SERVICE_PORT || 3001;
const url = `mongodb://${process.env.MONGO_USER || 'root'}:${process.env.MONGO_PASSWORD || 'example'}@${process.env.MONGO_SERVER || 'localhost'}:27017`

const mongodb = config.get('mongoURI');

const pubsub = new PubSub()

const schema = buildFederatedSchema([{ typeDefs, resolvers }])

const server = new ApolloServer({
  schema,
  context: req => ({
    ...req,
    pubsub,
  }),
  playground: {
    endpoint: '/graphql',
  },
})

// mongoose.connect( mongodb, {useNewUrlParser: true}, function (err){
//   if(err) {
//     console.log('unable to connect to database.');
//   } else{
//     server
//       .listen({port: port})
//       .then(({mongodb}) => {
//         console.log(`MongoDB connected`)
//         console.log(`Server is running on localhost:${port}`)
//       })
//   }
// });


db.connect(url, err => {
  if(err){
    console.log('Unable to connect to database')
  }else{
    server
      .listen({port: port})
      .then(({url}) => {
        console.log(`MongoDB connected`)
        console.log(`Server is running on ${url}`)
      })
  }
})


// db.connect(url, err => {
//   if (err) {
//     console.log('Unable to connect to database.')
//   } else {
//     server
//       .listen({ port: (process.env.SERVICE_PORT || config.app.port), endpoint: config.app.endpoint })
//       .then(({ url, subscriptionsPath }) => {
//         console.log(url)
//         console.log(subscriptionsPath)
//         console.log(`Server is running on ${url}`)
//       })
//   }
// })
